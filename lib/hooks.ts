import { useEffect, useState } from "react";
import { auth, googleAuthProvider, saveUser } from "./firebase";
import { useAuthState } from "react-firebase-hooks/auth";
import CoinsRepository from "./coins.repository";
import { IMarketCoin, ISubscription } from "../models/Coin";
import { createDateFormatted } from "./dates";

// Redux
import { useDispatch, useSelector } from "react-redux";
import { fetchAllMarketCoins } from "../store/slices/coins";
import { fetchAllSubscriptions } from "../store/slices/subscriptions";
import { getUserLogged, logoutUser } from "../store/slices/user";

export function useUser() {
  const [user] = useAuthState(auth);

  const { currentUser: userState } = useSelector((state: any) => state.user);
  const dispatch = useDispatch();

  useEffect(() => {
    if (user) {
      dispatch(getUserLogged() as any);
    }
  }, [user, dispatch]);

  const login = async (email: string, password: string) => {
    const res = await auth.signInWithEmailAndPassword(
      email.toString(),
      password.toString()
    );
    if (res.user) await saveUser(res.user);
  };

  const register = async (data: {
    name: string;
    email: string;
    password: string;
  }) => {
    const { name, email, password } = data;
    const res = await auth.createUserWithEmailAndPassword(email, password);
    if (!res.user) return;
    const user = { ...res.user };
    user.displayName = name;
    await saveUser(user);
  };

  const loginWithGoogle = async () => {
    const res = await auth.signInWithPopup(googleAuthProvider);
    if (res.user) await saveUser(res.user);
  };

  return {
    user: userState,
    register,
    login,
    loginWithGoogle,
    logout: () => {
      dispatch(logoutUser() as any);
    },
  };
}

export function useCoins() {
  const { list: coins }: { list: IMarketCoin[] } = useSelector(
    (state: any) => state.coins
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAllMarketCoins() as any);
  }, [dispatch]);

  return { coins };
}

export function useSubscriptions(userId: string | undefined) {
  const { list: subscriptions }: { list: undefined | ISubscription[] } =
    useSelector((state: any) => state.subscriptions);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!userId) return;
    dispatch(fetchAllSubscriptions(userId) as any);
  }, [userId, dispatch]);

  return { subscriptions };
}

export function useCoinChartData(id: string | undefined) {
  const [prices, setPrices] = useState<number[]>();
  const [dates, setDates] = useState<string[]>();

  useEffect(() => {
    if (!id) return;
    const req = async () => {
      const res = await CoinsRepository.fetchCoinChartData(id);
      const data = res.data;

      const _dates: string[] = [];
      const _prices: number[] = [];
      for (const [date, price] of data.prices) {
        _dates.push(createDateFormatted(new Date(date)));
        _prices.push(Number(price.toFixed(2)));
      }

      setPrices(_prices);
      setDates(_dates);
    };

    req();
  }, [id]);

  return { dates, prices };
}
