import axios from "axios";
import {
  createCoinAddapted,
  createCoinMarketAddapted,
} from "../addapters/coin.addapter";
import { ICoinChart } from "../components/coin-chart";
import { IMarketCoin, ICoin, ISubscription } from "../models/Coin";
import { firestore } from "./firebase";

const baseUrl = "https://api.coingecko.com/api/v3";

// EXAMPLE URL
// https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd

async function fetchAll() {
  const res = await axios
    .get<IMarketCoin[]>(baseUrl + "/coins/markets?vs_currency=usd")
    .then((v) => ({
      ...v,
      data: v.data.map((i) => createCoinMarketAddapted(i)),
    }));

  return res;
}

async function fetchOne(id: string) {
  const res = await axios.get<ICoin>(baseUrl + "/coins/" + id).then((v) => ({
    ...v,
    data: createCoinAddapted(v.data),
  }));

  return res;
}

async function fetchCoinChartData(id: string) {
  const res = await axios.get<ICoinChart>(
    `${baseUrl}/coins/${id}/market_chart?vs_currency=usd&days=30&interval=daily`
  );
  return res;
}

async function fetchSubscriptions(userId: string) {
  const ref = await firestore
    .collection("subscriptions")
    .where("userId", "==", userId)
    .get();

  const subscriptions: ISubscription[] = [];

  for (const doc of ref.docs) {
    const data = doc.data() as { id: string; coinId: string; userId: string };
    const sub = await fetchOne(data.coinId);
    subscriptions.push({
      ...data,
      coin: sub.data,
    });
  }

  return subscriptions;
}

async function fetchOneSubscription(userId: string, coinId: string) {
  const ref = await firestore
    .collection("subscriptions")
    .where("userId", "==", userId)
    .where("coinId", "==", coinId)
    .get();

  if (ref.empty) return null;
  const doc = ref.docs[0];
  const data = doc.data() as { id: string; coinId: string; userId: string };
  const sub = await fetchOne(data.coinId);

  return {
    ...data,
    coin: sub.data,
  };
}

async function subscribe({
  coinId,
  userId,
}: {
  coinId: string;
  userId: string;
}): Promise<ISubscription> {
  const ref = firestore.collection("coins").doc(coinId);
  await ref.set({
    id: coinId,
  });

  const subRef = await firestore.collection("subscriptions").add({
    coinId,
    userId,
  });

  const subscription = await subRef.get();
  return subscription.data() as ISubscription;
}

const CoinsRepository = {
  fetchAll,
  fetchOne,
  subscribe,
  fetchSubscriptions,
  fetchOneSubscription,
  fetchCoinChartData,
};

export default CoinsRepository;
