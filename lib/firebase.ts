import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";
import createUserAddapted from "../addapters/user.addapter";
import { IUser } from "../models/User";

const firebaseConfig = {
  apiKey: "AIzaSyAf4pfMWgQeP7kPcdroHqIh4r2UWCCExlc",
  authDomain: "crypto-tracker-afce1.firebaseapp.com",
  projectId: "crypto-tracker-afce1",
  storageBucket: "crypto-tracker-afce1.appspot.com",
  messagingSenderId: "97218515694",
  appId: "1:97218515694:web:b9da1008e4c30da37ed9ff",
  measurementId: "G-KQJF2C21EZ",
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export const auth = firebase.auth();
export const firestore = firebase.firestore();

// FIREBASE AUTH PROVIDERS
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export async function logout() {
  console.log("CLOSING SESSION");
  await auth.signOut();
  console.log("SESSION CLOSED SUCCESSFULLY");
}

export async function saveUser(user: firebase.User) {
  const ref = firestore.collection("users").doc(user.uid);
  const doc = await ref.get();
  if (doc.exists) return doc.data() as IUser;

  await ref.set(createUserAddapted(user));
  const saved = await ref.get();
  return saved.data() as IUser;
}
