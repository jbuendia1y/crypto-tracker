export function createDateFormatted(date: Date) {
  const intl = new Intl.DateTimeFormat();
  return intl.format(date);
}
