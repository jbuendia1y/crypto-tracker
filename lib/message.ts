import { ICoin, ISubscription } from "../models/Coin";
import { firestore } from "./firebase";

export async function getAllSubscriptionsCoins() {
  const ref = await firestore.collection("subscriptions").get();

  const coins: {
    [id: string]: ICoin;
  } = {};

  for (const doc of ref.docs) {
    const data = doc.data() as ISubscription;
    if (coins[data.coinId]) continue;

    const coinDoc = await firestore.collection("coins").doc(data.coinId).get();
    coins[data.coinId] = coinDoc.data() as ICoin;
  }

  return coins;
}
