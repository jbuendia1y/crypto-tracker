import { createContext } from "react";
import { IUser } from "../models/User";

export const UserContext = createContext<{
  user: IUser | null | undefined;
}>({
  user: null,
});
