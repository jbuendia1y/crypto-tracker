import Image from "next/image";
import { useUser } from "../lib/hooks";

export function SignInGoogleButton() {
  const { loginWithGoogle } = useUser();

  const signInWithGoogle = async () => {
    await loginWithGoogle();
  };

  return (
    <div className="d-grid gap-2">
      <button className="btn btn-light rounded-pill" onClick={signInWithGoogle}>
        <Image width={25} height={25} src={"/google.png"} alt="google" /> Sign
        in with Google
      </button>
    </div>
  );
}
