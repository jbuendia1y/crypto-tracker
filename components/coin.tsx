import Image from "next/image";
import Link from "next/link";
import { ICoin, IMarketCoin } from "../models/Coin";

export default function CoinCard({
  coin,
  subscribe,
}: {
  coin: IMarketCoin | ICoin;
  subscribe?: (coinId: string) => void;
}) {
  return (
    <div className="card flex-row p-1 justify-content-between">
      <div className="d-flex align-items-center">
        <Image
          width={20}
          height={20}
          src={coin.image instanceof Object ? coin.image.thumb : coin.image}
          alt={coin.name}
        />
        <div className="ms-2">
          <h2 className="h6 m-0">{coin.name}</h2>
          <p className="m-0">
            Price: $
            {(coin as IMarketCoin).current_price
              ? (coin as IMarketCoin).current_price
              : (coin as ICoin).market_data.current_price.usd}
          </p>
        </div>
      </div>
      <Link href={"/coins/" + coin.id}>
        <a className="btn btn-sm btn-primary rounded-pill d-flex align-items-center">
          Ver más
        </a>
      </Link>
    </div>
  );
}
