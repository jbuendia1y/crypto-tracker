import Image from "next/image";
import Link from "next/link";
import { useUser } from "../lib/hooks";

export default function Navbar() {
  const { user, logout } = useUser();

  const handleClick = () => {
    logout();
  };

  return (
    <nav className="navbar navbar-expand-lg bg-light">
      <div className="container-fluid">
        <Link href={"/"}>
          <a className="navbar-brand">Cryto-tracker</a>
        </Link>
        <div>
          {user === undefined && <span>Loading ...</span>}
          {user === null && (
            <ul className="navbar-nav">
              <li className="nav-item">
                <Link href={"/login"}>
                  <a className="nav-link">Iniciar sessión</a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href={"/singup"}>
                  <a className="nav-link">Registrarse</a>
                </Link>
              </li>
            </ul>
          )}
          {user && user !== undefined && (
            <div className="d-flex align-items-center">
              <Link href="/subscriptions">
                <a className="nav-link">Subscripciones</a>
              </Link>
              <button onClick={handleClick} className="btn nav-link">
                Cerrar sessión
              </button>
              <div className="ms-2">
                <Image
                  className="rounded-circle"
                  src={user.avatar || "/vercel.svg"}
                  alt={user.name}
                  width={50}
                  height={50}
                />
              </div>
            </div>
          )}
        </div>
      </div>
    </nav>
  );
}
