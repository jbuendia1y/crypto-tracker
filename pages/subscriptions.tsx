import CoinCard from "../components/coin";
import { useSubscriptions, useUser } from "../lib/hooks";
import { Spinner } from "../ui";

export default function Subscriptions() {
  const { user } = useUser();
  const { subscriptions } = useSubscriptions(user?.id);

  if (!subscriptions) {
    return (
      <main className="container d-grid w-100 h-100 place-content-center">
        <Spinner width={"5rem"} height={"5rem"}></Spinner>
      </main>
    );
  }

  return (
    <main className="container-sm d-grid gap-2">
      {subscriptions.map((v, i) => (
        <CoinCard coin={v.coin} key={v.coin.id + i} />
      ))}
    </main>
  );
}
