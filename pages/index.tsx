import type { NextPage } from "next";
import Head from "next/head";
import CoinCard from "../components/coin";
import { useCoins } from "../lib/hooks";

const Home: NextPage = () => {
  const { coins } = useCoins();

  return (
    <div className="container d-grid gap-2">
      <Head>
        <title>Crypto-tracker</title>
      </Head>
      {coins ? (
        coins.map((i) => <CoinCard coin={i} key={i.id} />)
      ) : (
        <span>Loading...</span>
      )}
    </div>
  );
};

export default Home;
