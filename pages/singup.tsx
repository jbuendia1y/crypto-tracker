import styles from "../styles/Login.module.css";
import { FormEvent } from "react";
import { useUser } from "../lib/hooks";
import { FormControl } from "../ui";
import { SignInGoogleButton } from "../components/signin-google-button";

export default function Singup() {
  const { register } = useUser();

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const { name, email, password } = Object.fromEntries(
      new FormData(event.target as any)
    );

    await register({
      name: name.toString(),
      email: email.toString(),
      password: password.toString(),
    });
  };

  return (
    <main className={styles.main}>
      <h1>Registro</h1>
      <form onSubmit={handleSubmit}>
        <FormControl
          label="Nombre"
          id="name"
          name="name"
          placeholder="Example"
          type="text"
        />
        <FormControl
          label="Correo electrónico"
          id="email"
          name="email"
          placeholder="example@example.com"
          type="email"
        />
        <FormControl
          label="Contraseña"
          id="password"
          name="password"
          placeholder="*********"
          type="password"
        />
        <div className="d-grid gap-2">
          <button className="btn btn-primary rounded-pill">Registrarse</button>
        </div>
      </form>
      <div>
        <SignInGoogleButton />
      </div>
    </main>
  );
}
