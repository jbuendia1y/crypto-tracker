import styles from "../styles/Login.module.css";
import { FormControl } from "../ui";
import { FormEvent } from "react";
import { SignInGoogleButton } from "../components/signin-google-button";
import { useUser } from "../lib/hooks";

export default function Login() {
  const { login } = useUser();

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const { email, password } = Object.fromEntries(
      new FormData(event.target as any)
    );
    if (!email || !password) return;

    await login(email.toString(), password.toString());
  };

  return (
    <main className={styles.main}>
      <h1>Iniciar sessión</h1>
      <form onSubmit={handleSubmit}>
        <FormControl
          label="Correo electrónico"
          id="email"
          name="email"
          placeholder="example@example.com"
          type="email"
        />
        <FormControl
          label="Contraseña"
          id="password"
          name="password"
          placeholder="*********"
          type="password"
        />
        <div className="d-grid gap-2">
          <button className="btn btn-primary rounded-pill">
            Iniciar session
          </button>
        </div>
      </form>
      <div>
        <SignInGoogleButton />
      </div>
    </main>
  );
}
