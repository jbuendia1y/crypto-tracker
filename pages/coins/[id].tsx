import { NextPage } from "next";
import dynamic from "next/dynamic";
import Head from "next/head";
import Image from "next/image";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import CoinsRepository from "../../lib/coins.repository";
import { useUser } from "../../lib/hooks";
import { ICoin } from "../../models/Coin";
import { fetchOneSubscription } from "../../store/slices/subscriptions";

interface CoinPageProps {
  coin: ICoin;
}

const Chart = dynamic(() => import("../../components/coin-chart"), {
  ssr: false,
});

const CoinPage: NextPage<CoinPageProps> = ({ coin }) => {
  const { user } = useUser();
  const [hasSub, setHasSub] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!user) return;
    CoinsRepository.fetchOneSubscription(user.id, coin.id).then((data) => {
      setHasSub(!!data);
    });
  }, [dispatch, coin, user]);

  const subscribe = async (coinId: string) => {
    if (!user) return;
    await CoinsRepository.subscribe({
      coinId,
      userId: user.id,
    });
    setHasSub(true);
  };

  return (
    <main className="container">
      <Head>
        <title>{coin.name} - Coin-tracker</title>
      </Head>
      <Image width={100} height={100} src={coin.image.large} alt={coin.name} />
      <h1>{coin.name}</h1>
      {!hasSub ? (
        <button
          className="btn btn-primary"
          onClick={() => {
            subscribe(coin.id);
          }}
        >
          Subscribirse
        </button>
      ) : (
        <p>Ya estás suscrito</p>
      )}

      <Chart id={coin.id} />
    </main>
  );
};

export async function getServerSideProps(context: any) {
  const id = context.query.id;
  const res = await CoinsRepository.fetchOne(id);

  return {
    props: {
      coin: res.data,
    },
  };
}

export default CoinPage;
