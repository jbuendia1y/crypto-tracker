import "../styles/globals.css";
import "bootstrap/dist/css/bootstrap.css";

import type { AppProps } from "next/app";
import Navbar from "../components/navbar";
import { Provider } from "react-redux";
import store from "../store";
import Head from "next/head";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <Head>
        <meta name="theme-color" content="#ffffff" />
        <link rel="apple-touch-icon" href="/vercel.svg" />
        <link rel="manifest" href="manifest.json" />
      </Head>
      <div className="main-box">
        <Navbar></Navbar>
        <div>
          <Component {...pageProps} />
        </div>
      </div>
    </Provider>
  );
}

export default MyApp;
