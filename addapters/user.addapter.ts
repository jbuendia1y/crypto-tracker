import firebase from "firebase";
import { IUser } from "../models/User";

export default function createUserAddapted(user: firebase.User) {
  const formatted: IUser = {
    id: user.uid as string,
    avatar: user.photoURL as string,
    name: user.displayName as string,
    email: user.email as string,
    phone: user.phoneNumber as string,
  };

  return formatted;
}
