/** @type {import('next').NextConfig} */
const withPwa = require("next-pwa");
const runtimeCaching = require("next-pwa/cache");

const nextConfig = withPwa({
  pwa: {
    dest: "public",
    runtimeCaching,
    register: true,
    skipWaiting: true,

    disable: process.env.NODE_ENV === "development",
  },

  reactStrictMode: true,
  images: {
    domains: ["assets.coingecko.com", "lh3.googleusercontent.com"],
  },
});

module.exports = nextConfig;
