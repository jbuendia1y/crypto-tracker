import { configureStore } from "@reduxjs/toolkit";

import user from "./slices/user";
import coins from "./slices/coins";
import subscriptions from "./slices/subscriptions";

const store = configureStore({
  reducer: {
    user,
    coins,
    subscriptions,
  },
});

export default store;
