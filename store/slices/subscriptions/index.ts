import { createSlice } from "@reduxjs/toolkit";
import CoinsRepository from "../../../lib/coins.repository";
import { ISubscription } from "../../../models/Coin";

export const subscriptionsSlice = createSlice({
  name: "subscriptions",
  initialState: {
    list: undefined as ISubscription[] | undefined,
    subscription: undefined as ISubscription | undefined,
  },
  reducers: {
    setSubscriptionsList: (state, action) => {
      state.list = action.payload;
    },
  },
});

export const { setSubscriptionsList } = subscriptionsSlice.actions;

export default subscriptionsSlice.reducer;

export const fetchOneSubscription = (userId: string, coinId: string) => {
  return (dispatch: any) => {
    CoinsRepository.fetchOneSubscription(userId, coinId).then((data) => {
      dispatch(setSubscriptionsList(data));
    });
  };
};

export const fetchAllSubscriptions = (userId: string) => {
  return (dispatch: any) => {
    CoinsRepository.fetchSubscriptions(userId).then((data) => {
      dispatch(setSubscriptionsList(data));
    });
  };
};
