import { createSlice } from "@reduxjs/toolkit";
import { IUser } from "../../../models/User";
import { auth, firestore } from "../../../lib/firebase";
import createUserAddapted from "../../../addapters/user.addapter";

export const userSlice = createSlice({
  name: "user",
  initialState: {
    currentUser: null as null | undefined | IUser,
  },
  reducers: {
    setUser: (state, action) => {
      state.currentUser = action.payload;
    },
  },
});

export const { setUser } = userSlice.actions;

export default userSlice.reducer;

export const getUserLogged = () => {
  return (dispatch: any) => {
    if (!auth.currentUser) return;
    dispatch(setUser(undefined));
    const user = createUserAddapted(auth.currentUser);
    const ref = firestore.collection("users").doc(user.id);
    ref.get().then((doc) => {
      if (doc.exists) dispatch(setUser(doc.data()));
      else dispatch(setUser(null));
    });
  };
};

export const logoutUser = () => {
  return (dispatch: any) => {
    auth.signOut().then((v) => {
      dispatch(setUser(null));
    });
  };
};
