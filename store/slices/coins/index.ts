import { createSlice } from "@reduxjs/toolkit";
import CoinsRepository from "../../../lib/coins.repository";
import { IMarketCoin } from "../../../models/Coin";

export const coinsSlice = createSlice({
  name: "coins",
  initialState: {
    list: undefined as IMarketCoin[] | undefined,
  },
  reducers: {
    setCoinsList: (state, action) => {
      state.list = action.payload;
    },
  },
});

export const { setCoinsList } = coinsSlice.actions;

export default coinsSlice.reducer;

export const fetchAllMarketCoins = () => {
  return (dispatch: any) => {
    CoinsRepository.fetchAll()
      .then((res) => {
        dispatch(setCoinsList(res.data));
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
