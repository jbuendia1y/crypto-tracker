interface SpinnerProps {
  width?: string | number;
  height?: string | number;
  small?: boolean;
}

export function Spinner(props: SpinnerProps) {
  let boxClassName = "spinner-border";
  if (props.small) {
    boxClassName += " spinner-border-sm";
  }

  return (
    <div
      className={boxClassName}
      style={{
        width: props.width,
        height: props.height,
      }}
      role="status"
    >
      <span className="visually-hidden">Loading...</span>
    </div>
  );
}
