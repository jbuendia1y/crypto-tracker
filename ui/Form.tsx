import { forwardRef, InputHTMLAttributes } from "react";

interface FormControlProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string;
}

export const FormControl = forwardRef<HTMLInputElement, FormControlProps>(
  ({ itemRef, label, ...props }, ref) => (
    <div className="mb-3">
      <label htmlFor={props.id} className="form-label">
        {label}
      </label>
      <input
        {...props}
        id={props.id}
        type={props.type}
        className="form-control"
        ref={ref}
      />
    </div>
  )
);

FormControl.displayName = "FormControl";
